import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { DesignPageComponent } from './design-page/design-page.component';
import { ContributePageComponent } from './contribute-page/contribute-page.component';
import { UserResolver } from './core/user.resolver';
import { VotingPageComponent } from './voting-page/voting-page.component';
import { LogoutComponent } from './logout/logout.component';

const appRoutes: Routes = [
  {
    path: '',
    resolve: { user: UserResolver },
    children: [
      { path: '', component: LandingPageComponent },
      { path: 'logout', component: LogoutComponent },
      { path: 'design/:guid', component: DesignPageComponent },
      { path: 'contribute/:guid', component: ContributePageComponent },
      { path: 'voting/:guid', component: VotingPageComponent },
      { path: '**', component: PageNotFoundComponent }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false }
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
