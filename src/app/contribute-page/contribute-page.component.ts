import { Component, OnInit, OnDestroy} from '@angular/core';
import { Session } from '../shared/session';
import { Category } from '../shared/category';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/mergeMap';
import { User } from '../shared/user';
import { UserService } from '../core/user.service';
import { ThemeService } from '../core/theme.service';

@Component({
  selector: 'app-contribute-page',
  templateUrl: './contribute-page.component.html',
  styleUrls: ['./contribute-page.component.scss']
})
export class ContributePageComponent implements OnInit, OnDestroy {
  user: User;
  session: Session;
  categories: Category[] = [];
  private routeParamsSubscription: Subscription;

  constructor(private route: ActivatedRoute, private http: HttpClient, private userService: UserService, private themeService: ThemeService) { }

  ngOnInit() {
    this.user = this.userService.user;
    this.routeParamsSubscription = this.route.params.subscribe(params => {
      const guid = params['guid'];

      return Observable.forkJoin(
        this.http.get<Session>(`${environment.retrioService}/api/sessions/${guid}`),
        this.http.get<Category[]>(`${environment.retrioService}/api/categories/${guid}`)
      ).subscribe(data => {
        this.session = data[0];
        this.categories = data[1];
      });
    });
  }

  ngOnDestroy() {
    if (this.routeParamsSubscription) {
      this.routeParamsSubscription.unsubscribe();
    }
  }

  get buttonTheme() {
    return this.themeService.getTheme().button;
  }

}
