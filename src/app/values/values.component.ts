import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-values',
  templateUrl: './values.component.html',
  styleUrls: ['./values.component.scss']
})
export class ValuesComponent implements OnInit {
  values: string[];

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.http.get<string[]>(`${environment.retrioService}/api/values`)
      .subscribe((data) => {
        this.values = data;
      });
  }

}
