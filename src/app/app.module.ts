import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ValuesComponent } from './values/values.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AppRoutingModule } from './app-routing.module';
import { ColumnComponent } from './column/column.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { NavbarComponent } from './navbar/navbar.component';
import { DesignPageComponent } from './design-page/design-page.component';
import { CategoryComponent } from './category/category.component';
import { ContributePageComponent } from './contribute-page/contribute-page.component';
import { CardComponent } from './card/card.component';
import { CoreModule } from './core/core.module';

import { SessionService } from './shared/session.service';
import { ThemeService } from './core/theme.service';
import { VotingPageComponent } from './voting-page/voting-page.component';
import { LogoutComponent } from './logout/logout.component';

@NgModule({
  declarations: [
    AppComponent,
    ValuesComponent,
    PageNotFoundComponent,
    ColumnComponent,
    LandingPageComponent,
    NavbarComponent,
    DesignPageComponent,
    CategoryComponent,
    ContributePageComponent,
    CardComponent,
    VotingPageComponent,
    LogoutComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    CoreModule,
    NgbModule.forRoot()
  ],
  providers: [
    SessionService,
    ThemeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
