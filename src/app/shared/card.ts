export class Card {
  cardId: number;
  groupId: number;
  text: string;
  voteCount: number;

  constructor(public categoryId: number, public userId: string) { }
}
