import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { environment } from '../../environments/environment';
import { Session } from './session';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class SessionService {

  constructor(private http: HttpClient) { }

  createSession() {
    const session = new Session();
    return this.http.post<Session>(`${environment.retrioService}/api/sessions`, { body: session });
  }

}
