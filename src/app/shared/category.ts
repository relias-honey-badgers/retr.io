export class Category {
  editing = false;

  categoryId: number;
  sessionId: string;
  name: string;
  ordinal: number;

  constructor(sessionId: string, ordinal: number) {
    this.sessionId = sessionId;
    this.ordinal = ordinal;
  }
}
