import { Component, OnInit, Input, AfterViewInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { Card } from '../shared/card';
import { Observable } from 'rxjs/Observable';
import { HubService } from '../core/hub.service';
import marked from 'marked';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/fromEvent';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @ViewChild('textInput') textInput;
  @Input() editable: boolean;
  @Input() card: Card;
  @Output() remove = new EventEmitter();
  @Input() voting = false;
  private _editing = true;
  voted = false;

  get editing() {
    return this.editable && this._editing;
  }

  set editing(val) {
    this._editing = val;
  }

  get bgColor() {
    return `#${this.card.userId.slice(0, 6)}`;
  }

  get color() {
    return this.invertColor(this.bgColor, true);
  }

  constructor(private hubService: HubService) { }

  ngOnInit() {
  }

  get markup() {
    return marked(this.card.text || '');
  }

  ngAfterViewInit() {
    if (this.editing) {
      this.focusInput();

      Observable.fromEvent(this.textInput.nativeElement, 'keyup')
        .debounceTime(300)
        .subscribe((event: KeyboardEvent) => {
          this.hubService.updateCard(this.card);
        });
    }
  }

  addVote() {
    if (!this.voted) {
      this.hubService.registerVote(this.card);
      this.voted = true;
    }
  }

  focusInput() {
    this.textInput.nativeElement.focus();
  }

  delete() {
    if (this.editable) {
      this.remove.emit();
    }
  }

  private invertColor(hex, bw) {
    if (hex.indexOf('#') === 0) {
        hex = hex.slice(1);
    }
    // convert 3-digit hex to 6-digits.
    if (hex.length === 3) {
        hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
    }
    if (hex.length !== 6) {
        throw new Error('Invalid HEX color.');
    }
    var r = parseInt(hex.slice(0, 2), 16).toString(),
        g = parseInt(hex.slice(2, 4), 16).toString(),
        b = parseInt(hex.slice(4, 6), 16).toString();
    if (bw) {
        // http://stackoverflow.com/a/3943023/112731
        return (+r * 0.299 + +g * 0.587 + +b * 0.114) > 186
            ? '#000000'
            : '#FFFFFF';
    }
    // invert color components
    r = (255 - +r).toString(16);
    g = (255 - +g).toString(16);
    b = (255 - +b).toString(16);
    // pad each with zeros and return
    return "#" + this.padZero(r) + this.padZero(g) + this.padZero(b);
  }

  private padZero(str, len = null) {
    len = len || 2;
    var zeros = new Array(len).join('0');
    return (zeros + str).slice(-len);
  }
}
