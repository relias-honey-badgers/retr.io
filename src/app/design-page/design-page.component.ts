import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';

import { Session } from '../shared/session';
import { Category } from '../shared/category';
import { environment } from '../../environments/environment';
import { ThemeService } from '../core/theme.service';

@Component({
  selector: 'app-design-page',
  templateUrl: './design-page.component.html',
  styleUrls: ['./design-page.component.scss']
})
export class DesignPageComponent implements OnInit, OnDestroy {
  @ViewChild('titleInput') titleInput;
  editing = false;
  session: Session = null;
  categories: Category[] = [];
  private routeParamsSubscription: Subscription;

  constructor(private route: ActivatedRoute, private http: HttpClient, private themeService: ThemeService) { }

  ngOnInit() {
    this.routeParamsSubscription = this.route.params.subscribe(params => {
      const guid = params['guid'];

      Observable.forkJoin(
        this.http.get<Session>(`${environment.retrioService}/api/sessions/${guid}`),
        this.http.get<Category[]>(`${environment.retrioService}/api/categories/${guid}`)
      ).subscribe(data => {
        this.session = data[0];
        if (!this.session.title) {
          this.editing = true;
          setTimeout(() => { this.focusInput(); });
        }
        this.categories = data[1];
      });
    });
  }

  ngOnDestroy() {
    if (this.routeParamsSubscription) {
      this.routeParamsSubscription.unsubscribe();
    }
  }

  focusInput() {
    this.titleInput.nativeElement.focus();
  }

  addCategory() {
    const newCategory = new Category(this.session.sessionId, this.categories.length)

    this.http.post<Category>(`${environment.retrioService}/api/categories`, newCategory)
      .subscribe((category) => {
        category.editing = true;
        this.categories.push(category);
      });
  }

  removeCategory(index: number) {
    const category = this.categories[index];
    this.http.delete(`${environment.retrioService}/api/categories/${category.categoryId}`)
      .subscribe(() => {
        this.categories.splice(index, 1);
      });
  }

  save() {
    this.http.put(`${environment.retrioService}/api/sessions`, this.session)
      .subscribe(() => {
        this.editing = false;
      });
  }

  edit() {
    this.editing = true;
    setTimeout(() => { this.focusInput(); });
  }

  get buttonTheme() {
    return this.themeService.getTheme().button;
  }
}
