import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ThemeService } from '../core/theme.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  // @Input() theme: string;
  @Output() themeChange = new EventEmitter();
  theme: string;

  constructor(private themeService: ThemeService) { }

  ngOnInit() {
    this.theme = this.themeService.getTheme().navbar;
  }

  changeTheme(event: Event, scheme: number) {
    /* event.preventDefault();
    this.themeChange.emit(scheme); */
    this.themeService.setTheme(scheme);
    this.theme = this.themeService.getTheme().navbar;
  }
}
