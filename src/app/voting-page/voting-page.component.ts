import { Component, OnInit } from '@angular/core';
import { UserService } from '../core/user.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { Session } from '../shared/session';
import { Category } from '../shared/category';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-voting-page',
  templateUrl: './voting-page.component.html',
  styleUrls: ['./voting-page.component.scss']
})
export class VotingPageComponent implements OnInit {
  categories: Category[];
  session: Session;
  routeParamsSubscription: any;
  user: any;
  constructor(private userService: UserService, private route: ActivatedRoute, private http: HttpClient) { }

  ngOnInit() {
    this.user = this.userService.user;
    this.routeParamsSubscription = this.route.params.subscribe(params => {
      const guid = params['guid'];

      return Observable.forkJoin(
        this.http.get<Session>(`${environment.retrioService}/api/sessions/${guid}`),
        this.http.get<Category[]>(`${environment.retrioService}/api/categories/${guid}`)
      ).subscribe(data => {
        this.session = data[0];
        this.categories = data[1];
      });
    });
  }
}
