import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { User } from '../shared/user';
import { UserService } from './user.service';

@Injectable()
export class UserResolver implements Resolve<User> {

  constructor(private userService: UserService) {}
 
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any>|Promise<any>|any {
    return this.userService.ensureUser();
  }
}
