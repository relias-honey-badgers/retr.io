import { Injectable } from '@angular/core';

const themes = [
  {
    navbar: 'bg-blue navbar-dark',
    body: 'bg-dark text-white',
    button: 'bg-blue text-white'
  },
  {
    navbar: 'bg-pink navbar-dark',
    body: 'bg-dark text-white',
    button: 'bg-pink text-dark'
  },
  {
    navbar: 'bg-green navbar-dark',
    body: 'bg-dark text-white',
    button: 'bg-green text-white'
  },
  {
    navbar: 'bg-green navbar-dark',
    body: 'bg-light text-dark',
    button: 'bg-green text-white'
  },
  {
    navbar: 'bg-yellow navbar-dark',
    body: 'bg-red text-yellow',
    button: 'bg-yellow text-red'
  }
];

@Injectable()
export class ThemeService {
  currentTheme = 0;

  constructor() { }

  setTheme(themeId: number) {
    this.currentTheme = themeId;
  }

  getTheme() {
    return themes[this.currentTheme];
  }
}
