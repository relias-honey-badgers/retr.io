import { Injectable, EventEmitter } from '@angular/core';
import { HubConnection } from '@aspnet/signalr-client';
import { environment } from '../../environments/environment';
import { Card } from '../shared/card';

@Injectable()
export class HubService {
  private _cardHubConnection: HubConnection;
  cardAdded = new EventEmitter<Card>();
  cardUpdated = new EventEmitter<Card>();
  cardDeleted = new EventEmitter<Card>();

  constructor() {
    this._cardHubConnection = new HubConnection(`${environment.retrioService}/cards`);

    this._cardHubConnection.on('Add', (card: Card) => {
      console.log('card added', card);
      this.cardAdded.emit(card);
    });
    this._cardHubConnection.on('Update', (card: Card) => {
      console.log('card updated', card);
      this.cardUpdated.emit(card);
    });
    this._cardHubConnection.on('Delete', (card: Card) => {
      console.log('card deleted', card);
      this.cardDeleted.emit(card);
    });

    this._cardHubConnection.start();
  }

  addCard(card: Card) {
    this._cardHubConnection.invoke('Add', card);
  }

  updateCard(card: Card) {
    this._cardHubConnection.invoke('Update', card);
  }

  removeCard(card: Card) {
    this._cardHubConnection.invoke('Delete', card);
  }

  registerVote(card: Card) {
    this._cardHubConnection.invoke('Vote', card);
  }
}
