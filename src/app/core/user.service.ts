import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/of';
import { User } from '../shared/user';
import { environment } from '../../environments/environment';

@Injectable()
export class UserService {

  user: User;

  constructor(private http: HttpClient) { }

  ensureUser(): Observable<User> {
    let userId = atob(localStorage.getItem('user') || '');
    const subscription = (userId === '') ?
      this.http.post<User>(`${environment.retrioService}/api/users`, {}) :
      this.http.get<User>(`${environment.retrioService}/api/users/${userId}`);
    return subscription.do(user => {
      this.user = user;
      if (user) {
        localStorage.setItem('user', btoa(user.userId));
      }
      else {
        localStorage.removeItem('user');
      }
    });
  }

  logout(): void {
    this.user = null;
    localStorage.removeItem('user');
  }
}
