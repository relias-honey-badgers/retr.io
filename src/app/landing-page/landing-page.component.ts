import { Component, OnInit } from '@angular/core';
import { SessionService } from '../shared/session.service';
import { Router } from '@angular/router';
import { ThemeService } from '../core/theme.service';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {

  sessionId = '';

  constructor(private sessionService: SessionService, private router: Router, private themeService: ThemeService) {
  }

  ngOnInit() {
  }

  joinRetro() {
    this.router.navigate(['/contribute', this.sessionId]);
  }

  createRetro() {
    this.sessionService.createSession().subscribe(res => {
      this.router.navigate(['/design', res.sessionId]);
    });
  }

  getButtonTheme() {
    return this.themeService.getTheme().button;
  }

}
