import { Component, OnInit, Input, AfterViewInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Category } from '../shared/category';
import { Card } from '../shared/card';
import { environment } from '../../environments/environment';
import { HubService } from '../core/hub.service';
import { UserService } from '../core/user.service';
import { User } from '../shared/user';
import { ThemeService } from '../core/theme.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit, AfterViewInit {
  @ViewChild('nameInput') nameInput;
  @Input() editable: boolean;
  @Input() category: Category;
  @Output() remove = new EventEmitter();
  @Input() voting = false;
  cards: Card[] = [];
  user: User;
  @Input() designing: boolean;

  get editing() {
    return this.editable && this.category.editing && !this.voting;
  }

  set editing(val: boolean) {
    this.category.editing = val;
  }

  constructor(private http: HttpClient, private hubService: HubService, private userService: UserService, private themeService: ThemeService) { }

  ngOnInit() {
    this.user = this.userService.user;
    this.http.get<Card[]>(`${environment.retrioService}/api/cards/${this.category.categoryId}`)
      .subscribe(cards => {
        this.cards = cards;
      });

    this.hubService.cardAdded.subscribe((card: Card) => {
      if (card.categoryId === this.category.categoryId) {
        this.cards.push(card);
      }
    });
    this.hubService.cardUpdated.subscribe((card: Card) => {
      let index = this.cards.findIndex((value: Card) => value.cardId === card.cardId);
      if (index > -1) {
        this.cards.splice(index, 1, card);
      }
    });
    this.hubService.cardDeleted.subscribe((card: Card) => {
      let index = this.cards.findIndex((value: Card) => value.cardId === card.cardId);
      if (index > -1) {
        this.cards.splice(index, 1);
      }
    });
  }

  ngAfterViewInit() {
    if (this.editing) {
      this.focusInput();
    }
  }

  addCard() {
    const card = new Card(this.category.categoryId, this.user.userId);
    this.hubService.addCard(card);
  }

  removeCard(index: number) {
    const card = this.cards[index];
    this.hubService.removeCard(card);
    this.cards.splice(index, 1);
  }

  focusInput() {
    this.nameInput.nativeElement.focus();
  }

  save() {
    if (this.editable) {
      this.http.put(`${environment.retrioService}/api/categories`, this.category)
        .subscribe(() => {
          this.editing = false;
        });
    }
  }

  edit() {
    if (this.editable) {
      this.editing = true;
      setTimeout(() => { this.focusInput(); });
    }
  }

  delete() {
    if (this.editable) {
      this.remove.emit();
    }
  }

  get buttonTheme() {
    return this.themeService.getTheme().button;
  }
}
